




// 3. Create a fetch request using the GET method that will retrieve all the to do list items from JSON Placeholder API.
fetch('https://jsonplaceholder.typicode.com/todos').then((response) => response.json())
.then((json) => console.log(json));

// 4. Using the data retrieved, create an array using the map method to return just the title of every item and print the result in the console.

fetch('https://jsonplaceholder.typicode.com/todos')
.then(response => response.json())
.then(data => {
  const titles = data
    .filter(item => item.title)
    .map(item => item.title);
  console.log(titles);
});

/* 
another answer using .map only
fetch('https://jsonplaceholder.typicode.com/todos')
    .then(response => response.json())
    .then(json => json.map((data) => {return data.title}))
    .then(title_arr => console.log(title_arr.isArray(title_arr))) */


   

// 5. Create a fetch request using the GET method that will retrieve a single to do list item from JSON Placeholder API.

fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response) => response.json())
.then((json) => console.log(json));


// 6. Using the data retrieved, print a message in the console that will provide the title and status of the to do list item.

fetch('https://jsonplaceholder.typicode.com/todos/1')
    .then(response => response.json())
    .then(json => console.log(`The item ${[json.title]} on the list has a status of ${json.completed}`))
    


// 7. Create a fetch request using the POST method that will create a to do list item using the JSON Placeholder API.

fetch('https://jsonplaceholder.typicode.com/todos', {
    method: 'POST',
    headers: {
        'Content-type': 'application/json'
    },
    body: JSON.stringify({
        title:'NEW POST',
        body: 'Hello Ian',
        userId: 201
    })

}).then((response) => response.json()).then((json) => console.log(json));


/*
    8. Create a fetch request using the PUT method that will update a to do list item using the JSON Placeholder API.
    9. Update a to do list item by changing the data structure to contain the following properties:

- Title
- Description
- Status
- Date Completed
- User ID
 */

fetch('https://jsonplaceholder.typicode.com/todos/7', {
    method: 'PUT',
    headers: {
        'Content-type': 'application/json'
    },
    body: JSON.stringify({
        id: 7,
        title:'Updated POST',
        body: 'Hello Ian',
        userId: 7,
        description: 'This is an update method using PUT',
        status: 'pending', 
        dataCompleted: 'pending'
        
    })

}).then((response) => response.json()).then((json) => console.log(json));

/* 
10. Create a fetch request using the PATCH method that will update a to do list item using the JSON Placeholder API.
11. Update a to do list item by changing the status to complete and add a date when the status was changed.
*/
fetch('https://jsonplaceholder.typicode.com/todos/1', {
    method: 'PUT',
    headers: {
        'Content-type': 'application/json'
    },
    body: JSON.stringify({
        status: 'Completed', 
        dataCompleted: '1/11/2023'
        
    })

}).then((response) => response.json()).then((json) => console.log(json));


// 12 Create a fetch request using the DELETE method that will delete an item using the JSON Placeholder API.

fetch('https://jsonplaceholder.typicode.com/todos/1', {
  method: 'DELETE'
});