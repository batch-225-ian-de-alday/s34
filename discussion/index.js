// [SECTION] Javascript Synchronous vs Asynchronous
// Javascript is by default is synchronous meaning that only one statement is executed a time

// synchronous sample
// synchronous means that it will stop the code execution from the error line of javascript or what we called BLOCKING
console.log('Hello Ian');

console.log("Goodbye");


// Asynchronous means that we can proceed to execute other statements, while time consuming code is running in the background.
// Non-Blocking

// The FETCH API

/* 
    - Allows you to asynchronously request for a resource (data)
    - A "promise" is an object that represents the eventual completion ( or failure) of an asynchronous function and it's resulting value

    it returns
    - result or data
    - pending
    - error

    Syntax:
        fetch('URL')

*/

// Promise pending
console.log(fetch('https://jsonplaceholder.typicode.com/posts'));


// Syntax
/* 
    fetch('URL')
    .then ((response) => {satement});
*/

// Retrieving all post following the REST API (retrieve, /post, GET)
// By using the then ,ethod we can now check for the status of the promise

// The 'fetch' method will return a "promise" that resolves to a "Response" Object
// The 'then' method captures the "response" object and returns another 'promise' will eventually be "resolve" or "rejected"

fetch('https://jsonplaceholder.typicode.com/posts')
.then((response) => console.log(response.status));


// ================== With JSON RESPONSE ========

fetch('https://jsonplaceholder.typicode.com/posts')
// use the 'json' method from the 'response' object to convert data retrieve into JSON format to be used in our application
.then((response) => response.json())

// Print the converted JSON value from the "fetch" request
// using multiple "then" method creates a "promise chain"
.then((json) => console.log(json));



// [SECTION] Getting a specific post

// Retrieves a specific post allowing the REST API (retrieve, /post/:id, GET)

fetch('https://jsonplaceholder.typicode.com/posts/2').then((response) => response.json())
.then((json) => console.log(json));


// [SECTION] Creating a post

/* 
    fetch('URL', options)
    .then((response)=> {})
    .then((response)=> {})
*/

// Create a new post following the REST API (create, /posts/:id, POST)

fetch("https://jsonplaceholder.typicode.com/posts", {
  
    // Sets the method of the "Request" object to "POST" following REST API
	// Default method is GET
    method: 'POST',

    // Sets the header data of the "Request" object to be sent to the backend
	// Specified that the content will be in a JSON structure
    headers: {
        'Content-TYPE': 'application/json'
    },

    // Sets the content/body data of the "Request" object to be sent to the backend
	// JSON.stringify converts the object data into a stringified JSON
    body: JSON.stringify({
        title: 'New Post',
        body: 'Hello World',
        userID: 101
    })
}).then((response) => response.json()).then((json) => { console.log(json) });

// [Section] Updating a post using PUT method

// Updates a specific post following the Rest API (update, /posts/:id, PUT)
fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'PUT',
	headers: {
  	'Content-type': 'application/json',
	},
	body: JSON.stringify({
	  	id: 1,
	  	title: 'Updated post',
	  	body: 'Hello again!',
	  	userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));


// [Section] Updating a post using PATCH method

// Updates a specific post following the Rest API (update, /posts/:id, Patch)
// The difference between PUT and PATCH is the number of properties being changed
// PATCH is used to update the whole object
// PUT is used to update a single/several properties
fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'PUT',
	headers: {
  	'Content-type': 'application/json',
	},
	body: JSON.stringify({
	  	title: 'Corrected post',
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// [Section] Deleting a post

// Deleting a specific post following the Rest API (delete, /posts/:id, DELETE)
fetch('https://jsonplaceholder.typicode.com/posts/1', {
  method: 'DELETE'
});


